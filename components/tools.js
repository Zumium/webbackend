const Promise = require('bluebird');

exports.rawDataToHourlyAvg=(rawData,day)=>
  Promise.resolve(rawData)
  .then(raw=>classifyHourly(raw,day))
  .then(
    data=>data.map((eachGrp,idx)=>{
      if (eachGrp.length === 0) return {hour: idx+1,latency:false,pktloss:false};
      var sums=eachGrp.reduce(
        (a,b)=>{
          return {latency: a.latency+b.latency,pktloss: a.pktloss+b.pktloss};
        },
        {latency:0,pktloss:0});
      return {hour: idx+1,latency: sums.latency/eachGrp.length,pktloss: sums.pktloss/eachGrp.length};
    })
  );

exports.rawLatencyToHourlyAvg=(rawLatency,day)=>
  Promise.resolve(rawLatency)
  .then(raw=>classifyHourly(raw,day))
  .then(
    data=>data.map(grp=>{
      if(grp.length === 0) return false;
      return grp.map(each=>each.latency).reduce((a,b)=>a+b,0)/grp.length
    })
  );

exports.rawPktlossToHourlyAvg=(rawPktloss,day)=>
  Promise.resolve(rawPktloss)
  .then(raw=>classifyHourly(raw,day))
  .then(
    data=>data.map(grp=>{
      if(grp.length === 0) return false;
      return grp.map(each=>each.pktloss).reduce((a,b)=>a+b,0)/grp.length
    })
  );

const classifyHourly=(raw,day)=>{
  var classify=[];
  for (let i = 0; i < 24; i++) {
    classify[i]=[];
  }
  const HOUR_MILLISEC=60*60*1000;
  const dayStart=Date.parse(day+' 00:00:00');
  raw.forEach(each=>classify[parseInt((Date.parse(each.time)-dayStart)/HOUR_MILLISEC)].push(each));
  return classify;
};
