/*
推送管理
本文件负责响应用户的WebSocket连接请求，并为用户控制WebSocket的数据接收提供支持
用户在连接到服务器后，通过监听data时间接收数据
用户还可以使用switchto事件切换想要接收的节点
使用pause事件可以暂停接受
*/
const io=require('socket.io');

var pushServer=null;

exports.init=httpServer=>{
	pushServer=io(httpServer);

	pushServer.on('connection',socket=>{
		socket.current='paused-clients';
		socket.join(socket.current);

		socket.on('switchto',addr=>{
			socket.leave(socket.current);
			socket.join(addr);
			socket.current=addr;
		});
		socket.on('pause',()=>{
			socket.leave(socket.current);
			socket.join('paused-clients');
			socket.current='paused-clients';
		});
	});
};
exports.push=data=>{
	pushServer.to(data.address).emit('data',data);
	pushServer.to('all').emit('data',data)
}
