const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const Promise = require('bluebird');
const getUri = require('./comp-uri-gene');
const genError = require('./gene-error');
const tools = require('./tools');

var router = module.exports = express.Router();
router.use(bodyParser.json());

//用户请求处理路由模块

router.get('/ifaces',(req,res,next)=>
  request.get(
    {
      uri: getUri('NodeDiscoveror','/localinterfaces'),
      json: true
    },
    (err,response,body)=>{
      if(err) return next(err);
      if(response.statusCode !== 200) return next(genError(response.statusCode,response.statusMessage));
      res.json(body);
    }
  )
);
router.get('/nodes',(req,res,next)=>
  request.get(
    {
      uri: getUri('NodeDiscoveror','/nodes'),
      json: true
    },
    (err,response,body)=>{
      if(err) return next(err);
      if(response.statusCode !== 200) return next(genError(response.statusCode,response.statusMessage));
      res.json(body);
    }
  )
);
router.get('/data/:ipaddr',(req,res,next)=>
  Promise.resolve()
  .then(()=>{
    if (!req.query.day) throw genError(400,'Must specific queried date using \'day\' query');
  })
  .then(()=>new Promise(
    (resolve,reject)=>
      request.get(
        {
          uri: getUri('DataManager','/nodes/'+req.params.ipaddr),
          json: true,
          qs: {
            start: req.query.day+' 00:00:00',
            end: req.query.day+' 24:00:00'
          }
        },
        (err,response,body)=>{
          if(err) return reject(err);
          if(response.statusCode !== 200) return reject(genError(response.statusCode,body.message||response.statusMessage));
          resolve(body);
        }
      )
    )
  )
  .then(data=>tools.rawDataToHourlyAvg(data,req.query.day))
  .then(result=>res.json(result))
  .catch(next)
);

router.get('/data/:ipaddr/latency',(req,res,next)=>
  Promise.resolve()
  .then(()=>{
    if (!req.query.day) throw genError(400,'Must specific queried date using \'day\' query');
  })
  .then(
    ()=>new Promise(
      (resolve,reject)=>
        request.get(
          {
            uri: getUri('DataManager','/nodes/'+req.params.ipaddr+'/latency'),
            json: true,
            qs: {
              start: req.query.day+' 00:00:00',
              end: req.query.day+' 24:00:00'
            }
          },
          (err,response,body)=>{
            if(err) return reject(err);
            if(response.statusCode !== 200) return reject(genError(response.statusCode,body.message||response.statusMessage));
            resolve(body);
          }
        )
    )
  )
  .then(data=>tools.rawLatencyToHourlyAvg(data,req.query.day))
  .then(result=>res.json(result))
  .catch(next)
);

router.get('/data/:ipaddr/pktloss',(req,res,next)=>
  Promise.resolve()
  .then(()=>{
    if (!req.query.day) throw genError(400,'Must specific queried date using \'day\' query');
  })
  .then(
    ()=>new Promise(
      (resolve,reject)=>
        request.get(
          {
            uri: getUri('DataManager','/nodes/'+req.params.ipaddr+'/pktloss'),
            json: true,
            qs: {
              start: req.query.day+' 00:00:00',
              end: req.query.day+' 24:00:00'
            }
          },
          (err,response,body)=>{
            if(err) return reject(err);
            if(response.statusCode !== 200) return reject(genError(response.statusCode,body.message||response.statusMessage));
            resolve(body);
          }
        )
    )
  )
  .then(data=>tools.rawPktlossToHourlyAvg(data,req.query.day))
  .then(result=>res.json(result))
  .catch(next)
);

router.get('/data/:ipaddr/latest', (req,res,next)=>{
  new Promise((resolve,reject)=>
    request.get(
      {
        uri: getUri('DataManager','/nodes/'+req.params.ipaddr+'/latest'),
        json: true
      },
      (err,response,body)=>{
        if(err) return reject(err);
        if(response.statusCode !== 200) return reject(genError(response.statusCode,body.message||response.statusMessage));
        resolve(body);
      }
    )
  )
  .then(d=>res.json(d))
  .catch(next)
});


router.get('/data/:ipaddr/onlinerate', (req,res,next)=>{
  new Promise((resolve,reject)=>
    request.get(
      {
        uri: getUri('DataManager','/nodes/'+req.params.ipaddr+'/onlinerate'),
        json: true
      },
      (err,response,body)=>{
        if(err) return reject(err);
        if(response.statusCode !== 200) return reject(genError(response.statusCode,body.message||response.statusMessage));
        resolve(body);
      }
    )
  )
  .then(d=>res.json(d))
  .catch(next)
});
