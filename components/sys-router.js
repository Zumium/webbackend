const Promise=require('bluebird');
const express=require('express');
const bodyParser=require('body-parser');
const push=require('./push');

var router=module.exports=express.Router();
router.use(bodyParser.json());

router.post('/data',(req,res,next)=>{
	push.push(req.body);
	res.sendStatus(200);
});

router.delete('/',(req,res)=>{
	res.sendStatus(200);
	process.exit();
});
