const config=require('../config/config');

module.exports=(moduleName,path)=>'http://unix:'+config.get('componentAddrs')[moduleName]+':'+path;
