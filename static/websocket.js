function webSocket (node){
	var arraySpeedAB = [], arraySpeedAC = [], arraySpeedAD = [];
	var arrayBagAB = [], arrayBagAC = [], arrayBagAD = [];
	var socket = io('http://45.32.141.215:8099');
	switch(node){
		case "A" :
			var speedChartCanvasA=document.getElementById('nodeASpeedChart');
			var bagChartCanvasA=document.getElementById('nodeABagChart');
			var dataSpeedA = {
				labels:["1","5","10","15","20","25","30","35","40","45",
				"50","55","60","65","70","75","80","85","90","95","100","单位（s）"
				],

				datasets: [{
					label: "A->B 时延变化（ms）",
					fill:false,
				              borderWidth: 1.5,
				              lineTension: 0.1,
				              backgroundColor: "rgba(75,192,192,0.5)",
				              borderColor: "rgba(75,192,192,1)",
				              borderCapStyle: 'butt',
				              borderDash: [],
				              borderDashOffset: 0.0,
				              borderJoinStyle: 'miter',
				              pointBorderColor: "rgba(75,192,192,1)",
				              pointBackgroundColor: "#fff",
				              pointBorderWidth: 1,
				              pointHoverRadius: 3,
				              pointHoverBackgroundColor: "rgba(75,192,192,1)",
				              pointHoverBorderWidth: 2,
				              pointRadius: 2,
				              pointHitRadius: 10,
					data: [],
				              spanGaps: false,
				},{
					fill:false,
					label:"A->C 时延变化（ms）",
					lineTension: 0.1,
					backgroundColor:  'rgba(255,99,132,0.5)',
					borderWidth:1.5,
					borderColor: 'rgba(255,99,132,1)',
					pointBorderColor:  'rgba(255,99,132,1)',
					pointBackgroundColor:"#fff",
					pointHoverBackgroundColor:'rgba(255,99,132,1)',
					pointRadius:2,
					pointHoverRadius: 3,
					pointBorderWidth: 1,
					pointHoverBorderWidth: 2,
					pointRadius: 2,
				             	pointHitRadius: 10,
					data: [],
					spanGaps: false,
				},{
					fill:false,
				 	label:"A->D 时延变化（ms）",
				 	lineTension: 0.1,
					backgroundColor: 'rgba(255, 206, 86, 0.5)',
					borderWidth:1.5,
					borderColor: 'rgba(255, 206, 86, 1)',
					pointBorderColor: 'rgba(255, 206, 86, 1)',
					pointBackgroundColor: "#fff",
					pointHoverBackgroundColor:'rgba(255, 206, 86, 1)',
					pointRadius:2,
					pointHoverRadius: 3,
					pointBorderWidth: 1,
					pointHoverBorderWidth: 2,
					data: [],
				}]
			};
			let dataBagA = {
				labels:["1","5","10","15","20","25","30","35","40","45",
				"50","55","60","65","70","75","80","85","90","95","100","单位（s）"
				],

				datasets: [{
					label: "A->B 丢包率变化（ms）",
					fill:false,
				              borderWidth: 1.5,
				              lineTension: 0.1,
				              backgroundColor: "rgba(75,192,192,0.5)",
				              borderColor: "rgba(75,192,192,1)",
				              borderCapStyle: 'butt',
				              borderDash: [],
				              borderDashOffset: 0.0,
				              borderJoinStyle: 'miter',
				              pointBorderColor: "rgba(75,192,192,1)",
				              pointBackgroundColor: "#fff",
				              pointBorderWidth: 1,
				              pointHoverRadius: 3,
				              pointHoverBackgroundColor: "rgba(75,192,192,1)",
				              pointHoverBorderWidth: 2,
				              pointRadius: 2,
				              pointHitRadius: 10,
					data: [],
				              spanGaps: false,
				},{
					fill:false,
					label:"A->C 丢包率变化（ms）",
					lineTension: 0.1,
					backgroundColor:  'rgba(255,99,132,0.5)',
					borderWidth:1.5,
					borderColor: 'rgba(255,99,132,1)',
					pointBorderColor:  'rgba(255,99,132,1)',
					pointBackgroundColor:"#fff",
					pointHoverBackgroundColor:'rgba(255,99,132,1)',
					pointRadius:2,
					pointHoverRadius: 3,
					pointBorderWidth: 1,
					pointHoverBorderWidth: 2,
					pointRadius: 2,
				             	pointHitRadius: 10,
					data: [],
					spanGaps: false,
				},{
					fill:false,
				 	label:"A->D 丢包率变化（ms）",
				 	lineTension: 0.1,
					backgroundColor: 'rgba(255, 206, 86, 0.5)',
					borderWidth:1.5,
					borderColor: 'rgba(255, 206, 86, 1)',
					pointBorderColor: 'rgba(255, 206, 86, 1)',
					pointBackgroundColor: "#fff",
					pointHoverBackgroundColor:'rgba(255, 206, 86, 1)',
					pointRadius:2,
					pointHoverRadius: 3,
					pointBorderWidth: 1,
					pointHoverBorderWidth: 2,
					data: [],
				}]
			};

			var mySpeedChartA=new Chart(speedChartCanvasA,{
				type: 'line',
				responsive: 'true',
				data: dataSpeedA,
				options: {
					animation:false,
					scales: {
						yAxes: [{
							ticks: {
								max: 400,
								min: 0,
								stepSize: 10,
							}
						}]
					}
				}
			});
			var myBagChartA=new Chart(bagChartCanvasA,{
				type: 'line',
				responsive: 'true',
				data: dataBagA,
				options: {
					animation:false,
					scales: {
						yAxes: [{
							ticks: {
								max: 100,
								min: 0,
								stepSize: 5,
							}
						}]
					}
				}
			});
			//==================================================
			function updateA(data) {
				if (dataSpeedA.datasets[0].data.length >20) {

					dataSpeedA.datasets[0].data.shift();
					dataSpeedA.datasets[1].data.shift();
					dataSpeedA.datasets[2].data.shift();
					dataBagA.datasets[0].data.shift();
					dataBagA.datasets[1].data.shift();
					dataBagA.datasets[2].data.shift();
					dataSpeedA.datasets[0].data.push(data[0]);
					dataSpeedA.datasets[1].data.push(data[1]);
					dataSpeedA.datasets[2].data.push(data[2]);
					dataBagA.datasets[0].data.push(data[3]);
					dataBagA.datasets[1].data.push(data[4]);
					dataBagA.datasets[2].data.push(data[5]);

				}else{

					dataSpeedA.datasets[0].data.push(data[0]);
					dataSpeedA.datasets[1].data.push(data[1]);
					dataSpeedA.datasets[2].data.push(data[2]);
					dataBagA.datasets[0].data.push(data[3]);
					dataBagA.datasets[1].data.push(data[4]);
					dataBagA.datasets[2].data.push(data[5]);
				}
				mySpeedChartA.update();
				myBagChartA.update();
			}

			let cache = [0,0,0,0,0,0]
			socket.emit("switchto","all");
			socket.on("data",data=>{
				switch (data.address) {
					case "209.141.58.171":
						cache[0]=data.latency
						cache[3]=data.pktloss*100
						break;
					case "108.61.214.74":
						cache[1]=data.latency
						cache[4]=data.pktloss*100
						break;
					case "45.32.30.179":
						cache[2]=data.latency
						cache[5]=data.pktloss*100
						break;
				}
				updateA(cache)
			});

			break;
		case "B" :
			var speedChartCanvasB=document.getElementById('nodeBSpeedChart');
			var bagChartCanvasB=document.getElementById('nodeBBagChart');

			let dataSpeedB = {
				labels:["1","5","10","15","20","25","30","35","40","45",
				"50","55","60","65","70","75","80","85","90","95","100","单位（s）"
				],

				datasets: [{
					label: "A->B 时延变化（ms）",
					fill:false,
				              borderWidth: 1.5,
				              lineTension: 0.1,
				              backgroundColor: "rgba(75,192,192,0.5)",
				              borderColor: "rgba(75,192,192,1)",
				              borderCapStyle: 'butt',
				              borderDash: [],
				              borderDashOffset: 0.0,
				              borderJoinStyle: 'miter',
				              pointBorderColor: "rgba(75,192,192,1)",
				              pointBackgroundColor: "#fff",
				              pointBorderWidth: 1,
				              pointHoverRadius: 3,
				              pointHoverBackgroundColor: "rgba(75,192,192,1)",
				              pointHoverBorderWidth: 2,
				              pointRadius: 2,
				              pointHitRadius: 10,
					data: [],
				              spanGaps: false,
				}]
			};
			let dataBagB = {
				labels:["1","5","10","15","20","25","30","35","40","45",
				"50","55","60","65","70","75","80","85","90","95","100","单位（s）"
				],

				datasets: [{
					label: "A->B 丢包率变化（ms）",
					fill:false,
				              borderWidth: 1.5,
				              lineTension: 0.1,
				              backgroundColor: "rgba(75,192,192,0.5)",
				              borderColor: "rgba(75,192,192,1)",
				              borderCapStyle: 'butt',
				              borderDash: [],
				              borderDashOffset: 0.0,
				              borderJoinStyle: 'miter',
				              pointBorderColor: "rgba(75,192,192,1)",
				              pointBackgroundColor: "#fff",
				              pointBorderWidth: 1,
				              pointHoverRadius: 3,
				              pointHoverBackgroundColor: "rgba(75,192,192,1)",
				              pointHoverBorderWidth: 2,
				              pointRadius: 2,
				              pointHitRadius: 10,
					data: [],
				              spanGaps: false,
				}]
			};

			var mySpeedChartB=new Chart(speedChartCanvasB,{
				type: 'line',
				responsive: 'true',
				data: dataSpeedB,
				options: {
					animation:false,
					scales: {
						yAxes: [{
							ticks: {
								max: 400,
								min: 0,
								stepSize: 10,
							}
						}]
					}
				}
			});
			var myBagChartB=new Chart(bagChartCanvasB,{
				type: 'line',
				responsive: 'true',
				data: dataBagB,
				options: {
					animation:false,
					scales: {
						yAxes: [{
							ticks: {
								max: 100,
								min: 0,
								stepSize: 5,
							}
						}]
					}
				}
			});
			//==================================================
			function updateB(data) {
				if (dataSpeedB.datasets[0].data.length >20) {

					dataSpeedB.datasets[0].data.shift();
					dataBagB.datasets[0].data.shift();
					dataSpeedB.datasets[0].data.push(data[0]);
					dataBagB.datasets[0].data.push(data[1]);

				}else{

					dataSpeedB.datasets[0].data.push(data[0]);
					dataBagB.datasets[0].data.push(data[1]);
				}
				mySpeedChartB.update();
				myBagChartB.update();
			}

			socket.emit("switchto","209.141.58.171");
			socket.on("data",data=>updateB([data.latency,data.pktloss*100]));

			break;
		case "C" :
			var speedChartCanvasC=document.getElementById('nodeCSpeedChart');
			var bagChartCanvasC=document.getElementById('nodeCBagChart');
			let dataSpeedCC = {
				labels:["1","5","10","15","20","25","30","35","40","45",
				"50","55","60","65","70","75","80","85","90","95","100","单位（s）"
				],

				datasets: [{
					fill:false,
					label:"A->C 时延变化（ms）",
					lineTension: 0.1,
					backgroundColor:  'rgba(255,99,132,0.5)',
					borderWidth:1.5,
					borderColor: 'rgba(255,99,132,1)',
					pointBorderColor:  'rgba(255,99,132,1)',
					pointBackgroundColor:"#fff",
					pointHoverBackgroundColor:'rgba(255,99,132,1)',
					pointRadius:2,
					pointHoverRadius: 3,
					pointBorderWidth: 1,
					pointHoverBorderWidth: 2,
					pointRadius: 2,
				             	pointHitRadius: 10,
					data: [],
					spanGaps: false,
				}]
			};
			let dataBagC = {
				labels:["1","5","10","15","20","25","30","35","40","45",
				"50","55","60","65","70","75","80","85","90","95","100","单位（s）"
				],

				datasets: [{
					label: "A->C 丢包率变化（ms）",
					fill:false,
				              borderWidth: 1.5,
				              lineTension: 0.1,
				              backgroundColor: "rgba(75,192,192,0.5)",
				              borderColor: "rgba(75,192,192,1)",
				              borderCapStyle: 'butt',
				              borderDash: [],
				              borderDashOffset: 0.0,
				              borderJoinStyle: 'miter',
				              pointBorderColor: "rgba(75,192,192,1)",
				              pointBackgroundColor: "#fff",
				              pointBorderWidth: 1,
				              pointHoverRadius: 3,
				              pointHoverBackgroundColor: "rgba(75,192,192,1)",
				              pointHoverBorderWidth: 2,
				              pointRadius: 2,
				              pointHitRadius: 10,
					data: [],
				              spanGaps: false,
				}]
			};

			var mySpeedChartC=new Chart(speedChartCanvasC,{
				type: 'line',
				responsive: 'true',
				data: dataSpeedCC,
				options: {
					animation:false,
					scales: {
						yAxes: [{
							ticks: {
								max: 400,
								min: 0,
								stepSize: 10,
							}
						}]
					}
				}
			});
			var myBagChartC=new Chart(bagChartCanvasC,{
				type: 'line',
				responsive: 'true',
				data: dataBagC,
				options: {
					animation:false,
					scales: {
						yAxes: [{
							ticks: {
								max: 100,
								min: 0,
								stepSize: 5,
							}
						}]
					}
				}
			});
			//==================================================
			function updateC(data) {
				if (dataSpeedC.datasets[0].data.length >20) {

					dataSpeedC.datasets[0].data.shift();
					dataBagC.datasets[0].data.shift();
					dataSpeedC.datasets[0].data.push(data[0]);
					dataBagC.datasets[0].data.push(data[1]);

				}else{

					dataSpeedC.datasets[0].data.push(data[0]);
					dataBagC.datasets[0].data.push(data[1]);
				}
				mySpeedChartC.update();
				myBagChartC.update();
			}
			socket.emit("switchto","108.61.214.74");
			socket.on("data",data=>updateC([data.latency,data.pktloss*100]));

			break;
		case "D" :
			var speedChartCanvasD=document.getElementById('nodeDSpeedChart');
			var bagChartCanvasD=document.getElementById('nodeDBagChart');
			let dataSpeedD = {
				labels:["1","5","10","15","20","25","30","35","40","45",
				"50","55","60","65","70","75","80","85","90","95","100","单位（s）"
				],

				datasets: [{
					fill:false,
				 	label:"A->D 时延变化（ms）",
				 	lineTension: 0.1,
					backgroundColor: 'rgba(255, 206, 86, 0.5)',
					borderWidth:1.5,
					borderColor: 'rgba(255, 206, 86, 1)',
					pointBorderColor: 'rgba(255, 206, 86, 1)',
					pointBackgroundColor: "#fff",
					pointHoverBackgroundColor:'rgba(255, 206, 86, 1)',
					pointRadius:2,
					pointHoverRadius: 3,
					pointBorderWidth: 1,
					pointHoverBorderWidth: 2,
					data: [],
				}]
			};
			let dataBagD = {
				labels:["1","5","10","15","20","25","30","35","40","45",
				"50","55","60","65","70","75","80","85","90","95","100","单位（s）"
				],

				datasets: [{
					fill:false,
				 	label:"A->D 丢包率变化（ms）",
				 	lineTension: 0.1,
					backgroundColor: 'rgba(255, 206, 86, 0.5)',
					borderWidth:1.5,
					borderColor: 'rgba(255, 206, 86, 1)',
					pointBorderColor: 'rgba(255, 206, 86, 1)',
					pointBackgroundColor: "#fff",
					pointHoverBackgroundColor:'rgba(255, 206, 86, 1)',
					pointRadius:2,
					pointHoverRadius: 3,
					pointBorderWidth: 1,
					pointHoverBorderWidth: 2,
					data: [],
				              spanGaps: false,
				}]
			};

			var mySpeedChartD=new Chart(speedChartCanvasD,{
				type: 'line',
				responsive: 'true',
				data: dataSpeedD,
				options: {
					animation:false,
					scales: {
						yAxes: [{
							ticks: {
								max: 400,
								min: 0,
								stepSize: 10,
							}
						}]
					}
				}
			});
			var myBagChartD=new Chart(bagChartCanvasD,{
				type: 'line',
				responsive: 'true',
				data: dataBagD,
				options: {
					animation:false,
					scales: {
						yAxes: [{
							ticks: {
								max: 100,
								min: 0,
								stepSize: 5,
							}
						}]
					}
				}
			});
			//==================================================
			function updateD(data) {
				if (dataSpeedD.datasets[0].data.length >20) {

					dataSpeedD.datasets[0].data.shift();
					dataBagD.datasets[0].data.shift();
					dataSpeedD.datasets[0].data.push(data[0]);
					dataBagD.datasets[0].data.push(data[1]);

				}else{
					dataSpeedD.datasets[0].data.push(data[0]);
					dataBagD.datasets[0].data.push(data[1]);
				}
				mySpeedChartD.update();
				myBagChartD.update();

			}
			socket.emit("switchto","45.32.30.179");
			socket.on("data",data=>updateD([data.latency,data.pktloss*100]));
			break;
	}

}
