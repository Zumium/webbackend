canAB = document.getElementById("connectRateAB");
canAC = document.getElementById("connectRateAC");
canAD = document.getElementById("connectRateAD");

ctxAB = canAB.getContext("2d"); 
ctxAC = canAC.getContext("2d");
ctxAD = canAD.getContext("2d");

var radius = canAB.height / 4;
ctxAB.translate(radius * 2, radius * 2);
ctxAC.translate(radius * 2, radius * 2);
ctxAD.translate(radius * 2, radius * 2);
radius = radius * 0.95;

function writeText(ctx, text){
	ctx.font = "12px Microsoft Yahei";
	ctx.fillText(text, 0, 80);
}

function drawPieChart(ctx, radius, num, text){
	var addColors = [];
	addColors = ["#64dd17","#76ff03","#eeff41","#ff6f00","#f44336"];
	var gradient = "gradient";
	drawFace(ctx, num);
	writeText(ctx, text);	
}
function drawFace(ctx, num){

	var addColors = [];
	addColors = ["#64dd17","#76ff03","#eeff41","#ff6f00","#f44336"];
	drawGradientCircle(ctx, 0, 0, radius, -0.5 * Math.PI, 1.5 * Math.PI, addColors);

	//draw the text;
	ctx.textBaseline = "middle";
	ctx.textAlign = "center";
	ctx.font = "24px Microsoft Yahei";
	
	ctx.fillText(num + "%", 0, 0);
	var rate = (num / 100 * 2 - 0.5) * Math.PI ;

	ctx.beginPath();
	ctx.strokeStyle = "#ef9a9a";
	ctx.lineWidth = 0.1 * radius;
	ctx.arc(0, 0, radius, rate, 1.5 * Math.PI );
	ctx.stroke();



	
}
function drawGradientCircle(ctx,xc, yc, r, sa, ea, colors){
	var partAngle = (ea - sa) / (colors.length - 1) ;
	var angle = sa;
	var gradient = null;
	var startColor = null;
	    endColor = null;

	for (let i = 0; i < colors.length; i++){
		startColor = colors[i];
		endColor = colors[i + 1];
		if((i + 1) == colors.length) break;
		//if you want the end of the part has gradient to StartColor,change it to
		//endColor = colors[(i + 1) % colors.length];

		var xStart = xc + Math.cos(angle) * r;
		var xEnd = xc + Math.cos(angle + partAngle) * r;

		var yStart = yc + Math.sin(angle) * r;
		var yEnd = yc + Math.sin(angle + partAngle) * r;

		ctx.beginPath();

		gradient = ctx.createLinearGradient(xStart, yStart, xEnd, yEnd);
		gradient.addColorStop(0, startColor);
		gradient.addColorStop(1, endColor);

		ctx.strokeStyle = gradient;
		ctx.arc(xc, yc, r, angle, angle + partAngle);
		ctx.lineWidth = 0.1 * r;
		ctx.stroke();

		angle += partAngle;
	}
}
function changeNum (canvas,ctx,dataProvider, text ,ipaddr){
	
		let data=dataProvider.getData(ipaddr);
		let num=data.num;
		let target=data['target'];
		
		setInterval(function(){
			ctx.clearRect(-canvas.width, -canvas.height, canvas.width * 2, canvas.height * 2);
			var curVal = num;
				
			var speed = (target - curVal) / 10 ;
			speed = (speed > 0)? Math.ceil(speed):Math.floor(speed);
			num = curVal + speed;
			drawPieChart(ctx, radius, num, text);
			
			
		},30);
	}
class DataProvider {
	constructor() {
		this._data;
	}
	getData(){
		return this._data;
	}
	setData(data){
		this._data = data;
	}
}
var dataProviderB = new DataProvider();
var dataProviderC = new DataProvider();
var dataProviderD = new DataProvider();

var today = new Date();
var todayDate = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate();


//注释部分是测试用的，可以直接运行的，数据来自/data/monitor-data.json
//这里所需要的数据是getNodeAData()中所提供的connectRate部分。

//getDataFromBackend("/data/monitor-data.json")
getNodeAData(todayDate)
	.then(function(data){

		// dataProviderB.setData({num:0,target:data.nodeB.connectRate});
		// dataProviderC.setData({num:0,target:data.nodeC.connectRate});
		// dataProviderD.setData({num:0,target:data.nodeD.connectRate});
		dataProviderB.setData({num:0,target:data.dataConnectRateAB});
		dataProviderC.setData({num:0,target:data.dataConnectRateAC});
		dataProviderD.setData({num:0,target:data.dataConnectRateAD});
		
		
		changeNum(canAB, ctxAB, dataProviderB, "AB持续连通率", "nodeB");
		changeNum(canAC, ctxAC, dataProviderC, "AC持续连通率", "nodeC");
		changeNum(canAD, ctxAD, dataProviderD, "AD持续连通率","nodeD");

	},
	function(err){
		console.log("Error:  "+err);
	});