//此文件可以输出默认的历史数据
//getDataFromBackend函数的第一个参数是读入文件的地址
//文件中的数据格式是json格式，
//{
//	"dataSpeedAB": [234,221,222,189,188,176,214,178,111,231,234,256,
//			231,197,178,223,175,167,245,175,186,232,234,135],
//	"dataSpeedAC":  [134,121,122,119,128,76,114,78,111,131,134,156,
//			131,97,78,123,75,67,45,75,86,132,134,135],
//	"dataSpeedAD": [342,121,222,109,158,276,124,178,121,181,234,156,
//			231,197,278,213,125,267,145,275,86,232,364,335],
//	"dataBagAB": [23,12,42,23,21,3,21,4,14,53,23,43,
//			43,21,32,43,12,23,26,26,34,24,23,15],
//	"dataBagAC": [43,34,53,32,54,24,25,25,24,34,53,23,
//			27,25,35,26,27,45,24,35,45,27,24,37],
//	"dataBagAD": [32,2,23,23,1,2,32,14,13,15,18,17,
//			31,15,17,26,10,25,23,9,24,26,32,15]
//}
var today = new Date();
var todayDate = today.getFullYear() + "-0" + (today.getMonth()+1) + "-0" + today.getDate();

function getNodeAData(date) {
    return Promise.all([
            getDataFromBackend('/ajax/data/' + nameToIp["nodeB"], date)
            .then(d => {
                return {
                    dataSpeedAB: d.latency,
                    dataBagAB: d.pktloss*100,
                    dataConnectRateAB: 100
                };
            }),
            getDataFromBackend('/ajax/data/' + nameToIp["nodeC"], date)
            .then(d => {
                return {
                    dataSpeedAC: d.latency,
                    dataBagAC: d.pktloss*100,
                    dataConnectRateAC: 100
                };
            }),
            getDataFromBackend('/ajax/data/' + nameToIp["nodeD"], date)
            .then(d => {
                return {
                    dataSpeedAD: d.latency,
                    dataBagAD: d.pktloss*100,
                    dataConnectRateA: 100
                };
            })
        ])
        .then(ds => {
            data = {}
            ds.forEach(d => {
                for (var k in d) {
                    data[k] = d[k]
                }
            })
            return data
        })
}


getNodeAData(todayDate)
    .then(function(data) {
        var dataHistorySpeed = {
            labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
                "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "单位（s）"
            ],

            datasets: [{
                label: "A->B 时延变化（ms）",
                fill: false,
                borderWidth: 1.5,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.5)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 2,
                pointHitRadius: 10,
                data: data.dataSpeedAB,
                spanGaps: false,
            }, {
                fill: false,
                label: "A->C 时延变化（ms）",
                lineTension: 0.1,
                backgroundColor: 'rgba(255,99,132,0.5)',
                borderWidth: 1.5,
                borderColor: 'rgba(255,99,132,1)',
                pointBorderColor: 'rgba(255,99,132,1)',
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: 'rgba(255,99,132,1)',
                pointRadius: 2,
                pointHoverRadius: 3,
                pointBorderWidth: 1,
                pointHoverBorderWidth: 2,
                pointRadius: 2,
                pointHitRadius: 10,
                data: data.dataSpeedAC,
                spanGaps: false,
            }, {
                fill: false,
                label: "A->D 时延变化（ms）",
                lineTension: 0.1,
                backgroundColor: 'rgba(255, 206, 86, 0.5)',
                borderWidth: 1.5,
                borderColor: 'rgba(255, 206, 86, 1)',
                pointBorderColor: 'rgba(255, 206, 86, 1)',
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: 'rgba(255, 206, 86, 1)',
                pointRadius: 2,
                pointHoverRadius: 3,
                pointBorderWidth: 1,
                pointHoverBorderWidth: 2,
                data: data.dataSpeedAD,
            }]
        };
        var dataHistoryBag = {
            labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
                "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "单位（s）"
            ],

            datasets: [{
                label: "A->B 丢包率变化（ms）",
                fill: false,
                borderWidth: 1.5,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.5)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 2,
                pointHitRadius: 10,
                data: data.dataBagAB,
                spanGaps: false,
            }, {
                fill: false,
                label: "A->C 丢包率变化（ms）",
                lineTension: 0.1,
                backgroundColor: 'rgba(255,99,132,0.5)',
                borderWidth: 1.5,
                borderColor: 'rgba(255,99,132,1)',
                pointBorderColor: 'rgba(255,99,132,1)',
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: 'rgba(255,99,132,1)',
                pointRadius: 2,
                pointHoverRadius: 3,
                pointBorderWidth: 1,
                pointHoverBorderWidth: 2,
                pointRadius: 2,
                pointHitRadius: 10,
                data: data.dataBagAC,
                spanGaps: false,
            }, {
                fill: false,
                label: "A->D 丢包率变化（ms）",
                lineTension: 0.1,
                backgroundColor: 'rgba(255, 206, 86, 0.5)',
                borderWidth: 1.5,
                borderColor: 'rgba(255, 206, 86, 1)',
                pointBorderColor: 'rgba(255, 206, 86, 1)',
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: 'rgba(255, 206, 86, 1)',
                pointRadius: 2,
                pointHoverRadius: 3,
                pointBorderWidth: 1,
                pointHoverBorderWidth: 2,
                data: data.dataBagAD,
            }]
        };

        var historySpeedChartCanvas = document.getElementById('historySpeedChart');
        var historyBagChartCanvas = document.getElementById('historyBagChart');

        var historySpeedChart = new Chart(historySpeedChartCanvas, {
            type: 'line',
            responsive: 'true',
            data: dataHistorySpeed,
            options: {
                animation: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            max: 400,
                            min: 0,
                            stepSize: 10,
                        }
                    }]
                }
            }
        });
        var historyBagChart = new Chart(historyBagChartCanvas, {
            type: 'line',
            responsive: 'true',
            data: dataHistoryBag,
            options: {
                animation: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            max: 100,
                            min: 0,
                            stepSize: 5,
                        }
                    }]
                }
            }
        });

    }, function(err) {
        console.log(`error : ${err}`);
    });
