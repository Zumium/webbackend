var Calendar = function (divId){
	this.divId = divId;
	//set days of week
	this.Days = ["Sun","Mon","Tue","Wen","Thu","Fri","Sat"];
	//set month
	this.Month = ["January","February","March","April","May","June",
		          "July","August","September","October","November","December"];

	var date = new Date();
	this.currYear = date.getFullYear();
	this.currMon = date.getMonth();
	this.currDate = date.getDate();

};
Calendar.prototype.prevMonth = function(){
	if(this.currMon == 0){
		this.currMon = 11;
		this.currYear -= 1;
	}else{
		this.currMon -=1;
	};
	this.showCalendar();
};
Calendar.prototype.nextMonth = function(){
	if(this.currMon == 11){
		this.currMon = 0;
		this.currYear +=1;
	}else{
		this.currMon += 1;
	};
	this.showCalendar();
};
Calendar.prototype.showCalendar = function(){
	this.showMonth(this.currYear, this.currMon);
};
Calendar.prototype.showMonth = function(year, mon){
	var d = new Date()
	,firstDayofMonth = new Date(year, mon, 1).getDay()
	,lastDateofMonth = new Date(year, mon+1, 0).getDate()
	,lastDateofLastMonth = mon == 0? new Date(year-1,11, 0).getDate() : new Date(year, mon, 0).getDate();

	var html = "<table>";

	//write selected month and year
	html += "<thead><tr>";
	html += "<td>" + this.Month[mon] + " " + year +"</td>";
	html += "</tr></thead>";

	//write the header of calendar
	html += "<tr class='days'>" ;
	for(let i = 0; i < this.Days.length; i++){
		html += "<td>" + this.Days[i] + "</td>";
	};
	html += "</tr>";

	var c = 1;
	do{
		var dayOfWeek = new Date(year, mon, c).getDay();

		//write last month date
		if(dayOfWeek == 0){
			html += "<tr>";
		}else if( c == 1){
			html +="<tr>";
			let j = lastDateofLastMonth - firstDayofMonth + 1;
			for( let k = 0 ; k < firstDayofMonth; k++ ){
				html += "<td class='not-current'>" + j + "</td>";
				j++;
			}
		}

		//write this month date
		var check = new Date();
		if(check.getFullYear() == this.currYear && check.getMonth() == this.currMon &&
		   c == this.currDate){
				 if (this.currMon <10 ) {var Mon = "0" + (this.currMon+1);} else {Mon = this.currMon + 1;}
				 if (c<10) {var d = "0" +c;} else {d=c;}

			html += "<td class='today'><a id=" + this.currYear + "-"+ (this.currMon+1) + "-" +c + " href='##'>" + c + "</a></td>";
		}else{
			html += "<td class='normal'><a id=" + this.currYear +"-" +(this.currMon+1) + "-" + c + " href='##" + c  + "'>" + c + "</a></td>";
		}
		if ( dayOfWeek == 6) {
			html += "</tr>";
		}

		//if last day is not Sat, write next month date
		else if( c == lastDateofMonth ){
			let j = 1;
			for(dayOfWeek; dayOfWeek < 6; dayOfWeek++){
				html += "<td class='not-current'>" + j + "</td>";
				j++;
			};
		}
	c++;
	}while(c <= lastDateofMonth);

	html += "</table>";

	document.getElementById(this.divId).innerHTML = html;
};
// On Load of the window
function startCalendar() {
	// Start calendar
	var cal = new Calendar("divCal");
	cal.showCalendar();

	// Bind next and previous button clicks

	document.getElementById('btnNext').onclick = function() {
		cal.nextMonth();
	};
	document.getElementById('btnPrev').onclick = function() {
		cal.prevMonth();
	};

}
//use .on() to make sure that js execute after the Dom has romanced which is dynamicly created.
//without unbind(), this.id will return mutiple times
captureDate=null;
$(document).on("click", ".normal", function() {
	$("a").unbind().click(function() {
		captureDate=this.id;
		console.log(captureDate);
		//第一个参数为传入历史数据的文件地址
		getNodeAData(captureDate)
			.then(function(data){
				var dataHistorySpeed = {
					labels:["0","1","2","3","4","5","6","7","8","9","10","11",
					            "12","13","14","15","16","17","18","19","20","21","22","23","24","单位（s）"
					],

					datasets: [{
						label: "A->B 时延变化（ms）",
						fill:false,
					              borderWidth: 1.5,
					              lineTension: 0.1,
					              backgroundColor: "rgba(75,192,192,0.5)",
					              borderColor: "rgba(75,192,192,1)",
					              borderCapStyle: 'butt',
					              borderDash: [],
					              borderDashOffset: 0.0,
					              borderJoinStyle: 'miter',
					              pointBorderColor: "rgba(75,192,192,1)",
					              pointBackgroundColor: "#fff",
					              pointBorderWidth: 1,
					              pointHoverRadius: 3,
					              pointHoverBackgroundColor: "rgba(75,192,192,1)",
					              pointHoverBorderWidth: 2,
					              pointRadius: 2,
					              pointHitRadius: 10,
						data: data.dataSpeedAB,
					              spanGaps: false,
					},{
						fill:false,
						label:"A->C 时延变化（ms）",
						lineTension: 0.1,
						backgroundColor:  'rgba(255,99,132,0.5)',
						borderWidth:1.5,
						borderColor: 'rgba(255,99,132,1)',
						pointBorderColor:  'rgba(255,99,132,1)',
						pointBackgroundColor:"#fff",
						pointHoverBackgroundColor:'rgba(255,99,132,1)',
						pointRadius:2,
						pointHoverRadius: 3,
						pointBorderWidth: 1,
						pointHoverBorderWidth: 2,
						pointRadius: 2,
					             	pointHitRadius: 10,
						data: data.dataSpeedAC,
						spanGaps: false,
					},{
						fill:false,
					 	label:"A->D 时延变化（ms）",
					 	lineTension: 0.1,
						backgroundColor: 'rgba(255, 206, 86, 0.5)',
						borderWidth:1.5,
						borderColor: 'rgba(255, 206, 86, 1)',
						pointBorderColor: 'rgba(255, 206, 86, 1)',
						pointBackgroundColor: "#fff",
						pointHoverBackgroundColor:'rgba(255, 206, 86, 1)',
						pointRadius:2,
						pointHoverRadius: 3,
						pointBorderWidth: 1,
						pointHoverBorderWidth: 2,
						data: data.dataSpeedAD,
					}]
				};
				var dataHistoryBag = {
					labels:["0","1","2","3","4","5","6","7","8","9","10","11",
					            "12","13","14","15","16","17","18","19","20","21","22","23","24","单位（s）"
					],

					datasets: [{
						label: "A->B 丢包率变化（ms）",
						fill:false,
					              borderWidth: 1.5,
					              lineTension: 0.1,
					              backgroundColor: "rgba(75,192,192,0.5)",
					              borderColor: "rgba(75,192,192,1)",
					              borderCapStyle: 'butt',
					              borderDash: [],
					              borderDashOffset: 0.0,
					              borderJoinStyle: 'miter',
					              pointBorderColor: "rgba(75,192,192,1)",
					              pointBackgroundColor: "#fff",
					              pointBorderWidth: 1,
					              pointHoverRadius: 3,
					              pointHoverBackgroundColor: "rgba(75,192,192,1)",
					              pointHoverBorderWidth: 2,
					              pointRadius: 2,
					              pointHitRadius: 10,
						 data: data.dataBagAB,
					              spanGaps: false,
					},{
						fill:false,
						label:"A->C 丢包率变化（ms）",
						lineTension: 0.1,
						backgroundColor:  'rgba(255,99,132,0.5)',
						borderWidth:1.5,
						borderColor: 'rgba(255,99,132,1)',
						pointBorderColor:  'rgba(255,99,132,1)',
						pointBackgroundColor:"#fff",
						pointHoverBackgroundColor:'rgba(255,99,132,1)',
						pointRadius:2,
						pointHoverRadius: 3,
						pointBorderWidth: 1,
						pointHoverBorderWidth: 2,
						pointRadius: 2,
					             	pointHitRadius: 10,
						data: data.dataBagAC,
						spanGaps: false,
					},{
						fill:false,
					 	label:"A->D 丢包率变化（ms）",
					 	lineTension: 0.1,
						backgroundColor: 'rgba(255, 206, 86, 0.5)',
						borderWidth:1.5,
						borderColor: 'rgba(255, 206, 86, 1)',
						pointBorderColor: 'rgba(255, 206, 86, 1)',
						pointBackgroundColor: "#fff",
						pointHoverBackgroundColor:'rgba(255, 206, 86, 1)',
						pointRadius:2,
						pointHoverRadius: 3,
						pointBorderWidth: 1,
						pointHoverBorderWidth: 2,
						data: data.dataBagAD,
					}]
				};

				var historySpeedChartCanvas=document.getElementById('historySpeedChart');
				var historyBagChartCanvas=document.getElementById('historyBagChart');

				var historySpeedChart = new Chart(historySpeedChartCanvas,{
						type: 'line',
						responsive: 'true',
						data: dataHistorySpeed,
						options: {
							animation: false,
							scales: {
								yAxes: [{
									ticks: {
										max: 400,
										min: 0,
										stepSize: 10,
									}
								}]
							}
						}
					});
				var historyBagChart=new Chart(historyBagChartCanvas,{
					type: 'line',
					responsive: 'true',
					data: dataHistoryBag,
					options: {
						animation: false,
						scales: {
							yAxes: [{
								ticks: {
									max: 100,
									min: 0,
									stepSize: 5,
								}
							}]
						}
					}
				});
			},
			function(err){
				console.log(`error : ${err}`);
			});

	});
});
