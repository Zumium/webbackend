const nameToIp = {
	"nodeA" : "zumsite.cn",
	"nodeB" : "209.141.58.171",
	"nodeC" : "108.61.214.74",
	"nodeD" : "45.32.30.179"
}

var request;
function getDataFromBackend (url,date){
	return new Promise(function(resolve,reject){
		request = new XMLHttpRequest();

		if (date === null) {
			request.open("GET",url,true);
		}
		else {
			request.open("GET",url+"?day="+date,true);
		}
		request.responseType = 'text';
		request.send();
		request.onload = function(){
			if(request.readyState == 4 && request.status == 200){
				let data = JSON.parse(request.responseText);
				resolve(data);
			} else
				reject(new Error("false" + request.status));

		};

		// request.open("POST",url,true);

		// request.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		// request.send(JSON.stringify(date));
		// request.onreadystatechange = function(){
		// 	if(request.readyState === 4 && request.status ===200 ){
		// 		console.log("success to connect with server");
		// 		request.open("GET",url,true);
		// 		request.responseType = 'text';
		// 		request.send();
		// 		request.onload = function(){
		// 			if(request.readyState == 4 && request.status == 200){
		// 				let data = JSON.parse(request.responseText);
		// 				resolve(data);
		// 			} else
		// 				reject(new Error("false" + request.status));
		// 		};
		// 	}
		// 	else{
		// 		alert("false:" + request.status);
		// 	}
		// }
	});
}
