const express=require('express');
const Promise=require('bluebird');
const fs=Promise.promisifyAll(require('fs'));
const http=require('http');
const path=require('path');
const config=require('./config/config');
const sysRouter=require('./components/sys-router');
const userRouter=require('./components/user-router');
const pushServer=require('./components/push');
const sendready=require('./config/sendready');
const errorHandler=require('./components/error-handler');

var sysServer=express();
sysServer.use(sysRouter);

var httpServer=null;
var userServer=express();
userServer.use(express.static(path.join(__dirname,'static')));
userServer.use('/ajax',userRouter);
userServer.use(errorHandler);

Promise.resolve()
.then(
	()=>config.load()
)
.then(
	()=>fs.accessAsync(config.get('serverSocket'),fs.constants.F_OK)
)
.then(
	()=>fs.unlinkAsync(config.get('serverSocket')),
	err=>Promise.resolve()
)
.then(
	()=>sysServer.listen(config.get('serverSocket'))
)
.then(
	()=>{
		httpServer=http.Server(userServer);
		pushServer.init(httpServer);
		httpServer.listen(config.get('userPort'));
	}
)
.then(
	()=>sendready('WebBackend')
);

//Does nothing when receiving SIGQUIT
process.on('SIGQUIT',()=>'');
